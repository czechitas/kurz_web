---
title:  1. lekce ‒ Opakování
date:   2017-05-17
---

## HTML

- připomenout si základní HTML značky
- Tagy jsou buď uzavíratelné `<p></p>`, kdy otvírací a zavírací značka obalují nějaký obsah (včetně další elementů, a tedy značek). Nebo máme tagy neuzavíratelné, např `<img>`, které představují nějaký nahrazovaný obsah samy o sobě (nemají co obalovat). Případně nesou další informace, např. tag `<link>` nebo `<meta>` (oba v hlavičce souboru).
- při zanořování tagy nekřížíme:

```html
<!-- špatně -->
<p>Text s <a href="#">odkazem</p></a>

<!-- správně -->
<p>Text s <a href="#">odkazem</a></p>

```

### Struktura HTML dokumentu

- nezapomínat na doctype`

```html
<!DOCTYPE html>
<html>
    <head>
        <!-- hlavička, nezobrazuje se, ale ovlivňuje vzhled a chování stránky -->
    </head>
    <body>
        <!-- tělo, stať stránky: všecnho, co se má zobrazit, tj. obsah patří sem -->
    </body>
</html>
```

### Tagy

#### Nadpisy
- `<h1>‒<h6>`
- dodržujeme hierarchii
- nepřeskakujeme žádnou úroveň: po `<h2>` jedině `<h3>` ne `<h4>`, pokud nám nevyhovuje vzhled (`<h3>` by byla „moc velká“), řešíme pomocí CSS
- naopak lze nadpis v CSS následně skrýt (ve struktuře zůstává, tj. je přístupný pro roboty a čtečky obsahu), ale v designu by byl nadbytečný

#### Odstavce odrážkové seznamy

- `<p>` nelze vnořovat do sebe
- `<ul>, <ol>` obalují položky seznamu `li`
- `<ul>` se používá k tvorbě navigace

#### Odkazy

- `<a href="http://czechitas.cz">odkaz na Czechitas</a>`
- nezanořují se do sebe
- mohou obalovat další elementy, nebo skupiny elementů

```html
<a href="odkaz na detail novinky">
    <div>
        <h3>Nadpis novinky</h3>
        <p>Úryvek novinky…</p>
    </div>
</a>
```

#### Obrázky

- `<img src="cesta k obrázku" alt="zástupný text">`
- nezapomínat na atribut `alt`, je povinný, ale u dekorativních obrázků ho lze ponechat prázdný

### Atributy

- dávají možnost doplnit další informace od daném elementu (cíl odkazu, zdroj obrázku, název třídy)
- píšou se do otvírací značky:

```html
<tag atribut="hodnota atributu" dalsi-atribut="jeho hodnota">obsah</tag>
```

- ne všechny atributy lze doplnit všem tagům (`href`, `src`)
- `class` lze přidat všem tagům

## CSS

- předepisuje prvkům v HTML jak mají vypadat
- přesněji, nastavuje prvkům HTML různé vlastnosti a přiřazuje jim hodnoty
- všechny prvky mají tzv. výchozí (default) vlastnosti, jedná se o normální CSS, které je ovšem součástí prohlížeče

```css
kdo {
    co: jak;
}

selektor {
    vlastnost: hodnota;
    jina-vlastnost: hodnota;
    …
}
```

### Barvy

- vícero způsobů zápisu

<table>
    <thead>
        <tr>
            <th>slovo</th>
            <th>hexadecimálně</th>
            <th>barva</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>green</td>
            <td>#008000</td>
            <td style="background-color: #008000">&nbsp;</td>
        </tr>
        <tr>
            <td>crimson</td>
            <td>#ed143d</td>
            <td style="background-color: #ed143d">&nbsp;</td>
        </tr>
        <tr>
            <td>cornflowerblue</td>
            <td>#6495ed</td>
            <td style="background-color: #6495ed">&nbsp;</td>
        </tr>
    </tbody>
</table>


### Jednotky

- `px` pixel ‒ absolutní jednotka, neovlivní ji jiná nastavení (typicky rozměry obrázků)
- `%` procenta ‒ relativní jednotka, ovlivňují ji rodičovské prvky i sourozenci (typicky šířky bloků na stránce)
- `em` ‒ relativní jednotka vychází z nastavení `font-size` rodičovského elementu (typicky škála velikostí písem)

### Dědičnost

Barva písma, velikost a tlouštka písma a **některé** další vlastnosti se dědí. To nám šetří práci: stačí nastavit barvu písma prvku `<body>` a všechny zanořené prvky (tzn. všechny na stránce) ji zdědí, takže ji nemusíme nastavovat každému prvku zvlášť.

**Pozor** odkzazy <a> barvu písma nedědí.

### Box model

Všechny prvky na stránce, bez ohledu na to, jak se nám opticky jeví jsou obdélníky = boxy.
Jejich rozměry určují následující vlastnosti (postupujeme od nitra prvku směrem ven).

- `width`, `height` ‒ šířka a výška prvku, není-li zadána, určuje ji obsah (velikost obrázku, délka a velikost textu atp.)
- `padding` ‒ výplň, odsazení obsahu od ohraničení, není-li zadáno bývá obyčejně 0
- `border` ‒ ohraničení („rámeček“) kolem prvku, není-li zadáno, obvykle nulová tloušťka
- `margin` ‒ okraj, určuje odsazení od ostatních prvků, není-li zadáno, obvykle 0

#### Úplný vs zkrácený zápis

Obdélníky jsou čtverhranné, a tak i my můžeme nastavit různé hodnoty pro různé strany prvku:

Zápis `margin: 10px` říká, budiž okraj na všech čtyřech stranách 10 pixelů. Ale hodnoty můžeme nastavit následujcími způsoby:

```css
.box {
    margin: 10px; /* 1 hodnota nastaví shodně všechny 4 strany */
    margin: 10px 20px; /* 2 hodnoty: první nastavuje horní a spodní okraj a druhá levou a pravou stranu */
    margin: 10px 20px 30px; /* 3 hodnoty: totéž co 2 hodnoty, třetí naství extra spodní okraj (uvádíme pro úplnost) */
    margin: 10px 20px 30px 40px; /* 4 hodnoty: začíná se od horní strany po směru hodinových ručiček, tj. vlevo bude okraj 40px */

    /* často se můžeš setkat s takovým pravidlem */
    margin: 10px; /* všechny strany 10px okraj s*/
    margin-bottom: 25px; /* spodní okraj bude větší než zbylé 3; přebijeme předchozí pravidlo, kde se mimo jiné nastavuje spodní okraj 10px */
}
```
