---
title:  Nástroje pro vývojáře
date:   2017-05-17
---

Součástí všech dnešních prohlížečů jsou nástroje pro vývojáře, _developer tools_, nebo také _devtools_ (tak tomuto nástroji budeme říkat i nadále, je to nejkratší).

## Proč?

Při tvorbě webu se nám hodí vidět jakým způsobem čte a interpretuje náš kód prohlížeč. A právě to nám umožňují devtools. Pokud mezi vývojářem (tedy námi) a prohlížečem vznikne nějaké nedorozumění, vyříkáme si to a na pravou míru uvedeme právě pomocí devtools.

## Kdy?

- něco je rozbité
- zajímá mě, jak je to udělané (na cizím webu)
- ladíme web
- experimenty

Ve všech případech oceníme, že změny, které v kódu (v rámci devtools) provedeme, se okamžitě projeví na stránce.

**Pamatuj si:** v devtools pracujeme s verzí stránky, která se stáhla do tohoto konkrétního prohlížeče. Změny se nikam neukládají. Reloadem se všechny místní úpravy ztratí.

## Jak?

Devtools spustíme následujcími způsoby:

- <kbd>F12</kbd>
- <kbd>Ctrl+Shift+I</kbd>, nebo <kbd>Cmd+Alt+I</kbd> (`I` jako inspect element)
- pravým myšítkem klikneš na prvek, který tě zajímá a z kontextové nabídky zvol _Prozkoumat_

## Co tam najdu

Když si otevřeš devtools do samostatného okna, měla bys vidět něco takového (pokud chybí spodní část, stiskni `Esc`, dalším stisknutím ji zase skryješ).

![Zjednodušené rozdělení okna](../assets/devtools/devtools-rozdeleni.png)

### HTML/DOM

<abbr title="Document Object Model">DOM</abbr> je to, co si prohlížeč sestaví z HTML kódu, který mu dodáme. V devtools si tak ověříme, jestli dodáváme srozumitelný kód.

![DOM](../assets/devtools/devtools-dom.png)

### CSS

Vlevo je panel se styly. Přeškrtnuté styly se neuplatní (někde se přebíjejí, pravděpodbně později v kódu). Kde máš hledat daná pravidla ti napoví název stylopisu a číslo řádku.
Nejvýše v panelu (a ve skutečnosti vespod kaskády) je místo pro rychlé úpravy stylů vybraného prvku (vybraného v pravé části devtools, nebo na stránce ‒ což není tak přesné).

![CSS](../assets/devtools/devtools-css.png)

### Přepínače

Úplně vlevo nahoře jsou 2 přepínače. První přepíná zvýrazňování prvků na stránce při pohybu myši. Druhý přepíná responzivní zobrazení (umožní si pohodlně zužovat/rozšiřovat stránku, nebo jí nastavit konkrétrní velikost v pixelech).
![Přepínače](../assets/devtools/devtools-switchers.png)

[zpět na úvod](/)
