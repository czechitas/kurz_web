---
title:  Formuláře
date:   2017-06-16
---

Webové stránky tvoříme téměř vždy s nějakým záměrem. Mají generovat zisk, sbírat a poskytovat informace nebo nám umožňují s návštěvníky komunikovat. Za tím vším často stojí formulář.

## Proč?

Formulář nám umožní od uživatele získat informace. Dobrý formulář se snadno vypňuje a je mnohem jednodušší a méně náročný na zpracování než emaily nebo hovory.

## Jak?

Formulář a všechen jeho obsah se nachází v elementu `form`. Může obsahovat textová pole, přepínače, zaškrtávátka, tlačítka a další vstupy.
Vstupy jsou nejčastěji nepárové elementy `input` s atributem `type`, který určuje typ vstupu, nebo mají svoji specifickou značku.
Vždy mají atribut `name`, jehož obsah slouží jako klíč zadané hodnoty, zatímco zadaná hodnota je až na vyjímky uložená v atributu `value`.
Inputům také přiřazujeme atribut `id` a tím mu dáváme jednoznačný identifikátor, protože na stránce nemohou být dva elementy se stejnou hodnotou atributu `id`.
Každý input by měl mít přiřazený párový element `label`. Jeho obsah popisuje, jaký účel má přiřazený input.
`label` má atribut `for`, do kterého zadáme stejnou hodnotu, jako má atribut `id` elementu `input`.
Pojďme si to níže vyzkoušet.

*Tip: Používej devtools a zkus měnit hodnoty atributů různých polí*

### Ukázka jednoduchého formuláře:
Nějak takhle by mohl vypadat jeden z nejjednodušších formulářů:
<form>
    <input type="text">
    <input type="submit">
</form>

```html

<form>
    <input type="text" name="name">
    <input type="submit">
</form>

```

## Inputy
Inputy

### Text
Nejzákladnější input je klasické textové pole (input type _text_). Do něj lze napsat jakýkoliv text (pokud nespecifikujeme jinak).

<input type="text" placeholder="Textové pole">

```html

<input type="text" placeholder="Textové pole">

```

### Number
Pokud potřebujeme od uživatele zadat číslo, pak použijeme input type _number_.
Je to textové pole, které se omezuje pouze na čísla. Většina moderních prohlížečů vám do něj nedovolí napsat nic jiného než číslo, nebo vám při špatné hodnotě nedovolí odeslat formulář.
Můžeme také použít atributy `min` a `max` pro omezení maximálního možného čísla. Atribut `step` nám říká, o kolik mají šipečky v inputu zvětšit nebo zmenšit číslo - ve výchozím stavu o 1.

<input type="number" placeholder="Pole na číslo" step="0.5" min="0" max="10">

```html

<input type="number" placeholder="Pole na číslo"
    step="0.5" <!-- Step určuje, o kolik se bude číslo zvětšovat či zmenšovat při mačkání šipek v inputu.
                    Navíc určuje i preciznost, jak se číslo bude zaokrouhlovat (validní číslo by mělo být
                    násobkem této hodnoty) -->
    min="0"    <!-- Min určuje, jaké nejmenší číslo můžeme do pole zadat -->
    max="10">  <!-- MAx určuje, jaké největší číslo můžeme do pole zadat -->

```

### Email &amp; tel
Pro zadání emailu a telefonního čísla použijeme input type _email_ a _tel_.

<input type="email" placeholder="Zadejte email">
<input type="tel" placeholder="Zadejte telefonní číslo">

```html

<input type="email" placeholder="Zadejte email">
<input type="tel" placeholder="Zadejte telefonní číslo">

```

### Radio &amp; checkbox
Pro případy, že chceme po uživateli vybrat jednu nebo více nabízených možností, použijeme inputy type _radio_ nebo _checkbox_. Radio inputy používáme ve skupince, ze které jde vždy vybrat jen jedna možnost. Uživatel si jednu z nich může vybrat a případně ji změnit, ale už nemůže výběr zrušit. Checkboxy jsou čtverečky, které můžeme zašrtnout a odškrtnout.
Oba tyto inputy nemají atribut `value`, ale když jsou žaškrtnuté, tak mají prázdný atribut `checked`

Radio inputy (nejčastěji se jim říká radiobuttony), které mají patřit k sobě, musí mít stejné jméno - atribut `name`.
V následujícím příkladu je vidět, že pro tyto dva inputy je opravdu důležité mít přiřazený `label`.
Navíc jsem použil elementy `fieldset` a `legend`. Ty pomáhají v orientaci ve větších formulářích a seskupují logicky související inputy.
Navíc pomáhají čtečkám pro slepé. Jinak nemají žádný funkční význam.

<fieldset>
<legend>Jaké víno je lepší</legend>

<label for="white-wine">Bílé</label>
<input id="white-wine" type="radio" name="better-wine" value="red">

<label for="red-wine">Červené</label>
<input id="red-wine" type="radio" name="better-wine" value="white" checked>
</fieldset>

<fieldset>
<label for="agreed">Souhlasím s licenčními podmínkami</label>
<input id="agreed" name="agreed" type="checkbox">

<label for="agreed-2">Souhlasím s ostatními podmínkami</label>
<input id="agreed-2" name="agreed" type="checkbox" checked>
</fieldset>

```html

<fieldset>
<legend>Jaké víno je lepší</legend>

<label for="white-wine">Bílé</label>
<input id="white-wine" type="radio" name="better-wine" value="red">

<label for="red-wine">Červené</label>
<input id="red-wine" type="radio" name="better-wine" value="white" checked>
</fieldset>

<fieldset>
<label for="agreed">Souhlasím s licenčními podmínkami</label>
<input id="agreed" name="agreed" type="checkbox">

<label for="agreed-2">Souhlasím s ostatními podmínkami</label>
<input id="agreed-2" name="agreed" type="checkbox" checked>
</fieldset>

```

### Textarea
Často chceme, aby nám uživatel napsal nějaký delší text. Na to je klasický textový input nevhodný a proto používáme párový element `textarea`

<textarea placeholder="Pokud chcete, můžete přidat poznámku ..."></textarea>

```html

<textarea placeholder="Pokud chcete, můžete přidat poznámku ..."></textarea>

```

### Select
Občas musí uživatel vybrat jednu nebo více možností z poměrně veliké nabídky.
Vypsat je všechny na stránce s radiobuttony by bylo nepřehledné, zabralo by to moc místa a uživatel by utek.
Proto použijeme párový element `select` ve kterém každý element `option` tvoří jednu položku (podobně jako `ul` a `li`).

<select name="color">
    <option value="red">Červená</option>
    <option value="green" selected>Zelená</option>
    <option value="blue">Modrá</option>
</select>

```html

<select name="color">
    <option value="red">Červená</option>
    <option value="green" selected>Zelená</option>
    <option value="blue">Modrá</option>
</select>

```

### Submit
Formulář potřebujeme nějak odeslat. K tomu nám slouží input type _submit_. Pomocí atributu value můžeme specifikovat text tlačítka.

<input type="submit">
<input type="submit" value="Odesílací tlačítko">

```html

<input type="submit">
<input type="submit" value="Odesílací tlačítko">

```

## Ukázkový formulář
Možná jste si už nahoře všimli, že některé inputy v sobě mají šedivý text. Může za to atribut `placeholder`.
I přesto, že s ním také můžete popsat pole, tak nezapomínejte k inputu připojit label, protože placeholder je vidět jen v případě, že pole není vyplněno.

Formuláři jsem také přidal atribut `method`, který určuje, jak se formulář bude odesílat.
Hodnota _post_ znamená, že se hodnoty formuláře odešlou v těle požadavku, hodnota _get_ znamená, že se zakódují a pošlou v url adrese jako ve formátu:

`?klíč=hodnota&amp;klíč2=hodnota2&amp;klíč3=hodnota3...`

Pokud atribut `method` vynecháte, tak bude nabývat výchozí hodnoty _get_.

Další důležitý atribut formuláře je `action`, který říká, na kterou adresu se má formulář posílat. Pokud ho nevyplníme, tak se pošle na aktuální adresu.

<form method="post">

<fieldset>
<legend>Kontaktní údaje</legend>

<div>
<label for="name">Jméno a příjmení</label>
<input type="text" id="name" name="name" placeholder="Zkus zadat 5 a více písmen" maxlength="5">
</div>

<div>
<label for="age">Věk</label>
<input type="number" id="age" name="age" placeholder="Něco mezi 18 a 99" min="18" max="99">
</div>

<div>
<label for="email">Email</label>
<input type="email" id="email" name="email" placeholder="email@email.cz" maxlength="5">
</div>

</fieldset>

<fieldset>
<legend>Rozšiřující informace</legend>

<div>
<label for="color">Barva vašeho automobilu</label>
<select id="color" name="color">
<option disabled selected>Vyberte</option>
<option value="red">Červená</option>
<option value="green">Zelená</option>
<option value="blue">Modrá</option>
</select>
</div>

</fieldset>

<fieldset>
<legend>Souhlasy</legend>

<div>
<label for="terms">Souhlasím s podmínkami serveru</label>
<input id="terms" name="terms" type="checkbox" required>
</div>

<div>
<label for="newsletter">Souhlasím se zasíláním newsletteru</label>
<input id="newsletter" name="newsletter" type="checkbox" checked>
</div>

</fieldset>

<div>
<input type="submit" value="Odeslat">
<input type="reset" value="Vymazat">
</div>

</form>



```html

<form method="post">

    <fieldset>
        <legend>Kontaktní údaje</legend>

        <div>
            <label for="name">Jméno a příjmení</label>
            <input type="text" id="name" name="name" placeholder="Zkus zadat 5 a více písmen" maxlength="5">
        </div>

        <div>
            <label for="age">Věk</label>
            <input type="number" id="age" name="age" placeholder="Něco mezi 18 a 99" min="18" max="99">
        </div>

        <div>
            <label for="email">Email</label>
            <input type="email" id="email" name="email" placeholder="email@email.cz" maxlength="5">
        </div>

    </fieldset>

    <fieldset>
        <legend>Rozšiřující informace</legend>

        <div>
            <label for="color">Barva vašeho automobilu</label>
            <select id="color" name="color">
                <option disabled selected>Vyberte</option>
                <option value="red">Červená</option>
                <option value="green">Zelená</option>
                <option value="blue">Modrá</option>
            </select>
        </div>

    </fieldset>

    <fieldset>
        <legend>Souhlasy</legend>

        <div>
            <label for="terms">Souhlasím s podmínkami serveru</label>
            <input type="checkbox" id="terms" name="terms" required>
        </div>

        <div>
            <label for="newsletter">Souhlasím se zasíláním newsletteru</label>
            <input type="checkbox" id="newsletter" name="newsletter" checked>
        </div>

    </fieldset>

    <label for="note">Zadejte volitelnou poznámku</label>
    <textarea id="note" name="note" placeholder="Pokud chcete, můžete přidat poznámku ..."></textarea>

    <div>
        <input type="submit" value="Odeslat">
        <input type="reset" value="Vymazat">
    </div>

</form>


```

## Validace
Jedna z nejdůležitějších věcí u formulářů je validace, to znamená kontrolování vstupních dat od uživatele.
Data jsou validní, pokud vyhovují všem podmínkám, které jsme nastavili - například pole na email musí obsahovat email, inputy s atributem `required` musí obsahovat hodnotu a tak dále.

Pár validačních pravidel najdete i ve formuláři výše (`min`, `max`, `maxlength`, `step`, `required` ...)

- [MDN - Seznam inputů a jejich podpora v prohlížečích](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input)
- [MDN - Validace formulářů pomocí HTML5 i JavaScriptu](https://developer.mozilla.org/en-US/docs/Learn/HTML/Forms/Form_validation)

[zpět na úvod](/)
