---
title:  4. lekce ‒ Pseudoelementy a pseudotřídy, pozicování, media queries
date:   2017-06-07
---

## Shrnutí lekce

### SVG obrázky

Při hledání vhodné ikony pro upoutávkový boxík o víně jsme se seznámili s webem [IconFinder.com](https://www.iconfinder.com/), kde si můžete stáhnout spousty ikon zdarma nebo za rozumnou cenu. Ikonu jsme přetvořili na `symbol` a přidali do sprite. Na ikoně pro ostrá jídla jsme si představili [online nástroj SVGOMG](https://jakearchibald.github.io/svgomg/), který vyčistí kód SVG od balastu, který vytvářejí grafické editory. Dále nám chilli paprička posloužila na ukázku ostylování různých částí kresby. Stopku jsme obarvili do zelena a papriku do červena.

### CSS

V krátkosti jsme se zastavili u generátorů barevných schémat ([Coolors.co](https://coolors.co/) a [Paletton.com](http://paletton.com/)). Pak jsme probírali pseudoelementy `::before` a `::after`, pseudotřídy `first-child` a `last-child`. Jejich pomocí jsme upravili vzhled úryvků recenzí, přičemž jsme si zopakovali relativní a absolutní pozicování.

Když jsme přidali obrázek předkrmu, pustili jsme se do responzivního ladění stránky. Rozšiřováním stránky v DevTools jsme řešili problematické prvky. Nastavili jsme tzv. breakpointy prostřednictvím media queries. V tom budeme pokračovat ještě příště.

### HTML

Vysvětlili jsme si rozdíl mezi bitmapovými obrázky a obrázky vektorovými (v křivkách. Naučili jsme se základní responzivní ošetření obrázků pomocí maximální šířky 100&nbsp;%. Zlepšili jsme také vzhled obrázky pro obrazovky s vyšší hustotou pixelů, když jsme nastavili `srcset` a přidali do něj další verze téhož obrázku. Obrázky jsme upravovali pomocí online nástrojů [Bulk Resize Photos](https://bulkresizephotos.com/) a [TinyJPG/PNG](https://tinyjpg.com/).


## Projekt

Doplnili jsme další ikony: chilli papričku pro ostrá jídla (obdobné umístění jako značka pro vegetariánské pokrmy) a otvírák do boxíku, kde se Al Dente chlubí, že má prvotřídní vína. Úryvkům recenzí jsme přidali zvětšené uvozovky (jako dekorativní prvek), odsadili odkaz „číst dál“ na nový řádek. Také jsme vytučnili jméno autora u jednotlivých recenzí. Pod sekci předkrmů jsme vložili obrázek cibulového salátu.

Písmo v záhlaví webu jsme pro nejužší obrazovky zmenšili a omezili logo maximální šířkou. Zkusil jsme si pracně upořádat upoutávkové boxíky tak, aby byly 2 vedle sebe, jakmile mají dost místa.

Postupně také vypínáme barevná podbarvení, abychom viděli web v konečných barvách.

---

## DÚ

- **SVG**
    - Na [IconFinder](https://www.iconfinder.com/) vyber ikonky ke zbylým upoutávkovým boxům. Vybírej s ohledem na následnou práci s ikonou: je třeba ji zanést do sprite, nepřidělávej si práci.
    - Stáhni si [ikonu hvězdy](../assets/downloads/lekce_03/star.svg). Je třeba ji vyčistit, nejlépe nástrojem [SVGOMG](https://jakearchibald.github.io/svgomg/). Úryvkům rezenzí pak vespod boxíku vytvoř 5hvězdičkové hodnocení. U každé recenze žlutě vybarvi jiný počet hvězdiček z 5 (zbylé vybarvi šedě).

- **Responzivní ladění**
    - Přidej nové pravidlo (media query) tak, aby na obrazovkách širších než 800 px, byly v jídelním lístku viditelná čísla alergenů.

- **Volal klient s návrhy na vylepšení**
    - někam vhodně umístit vysvětlivky k ikonám v jídeláku (vege a chilli paprička)
    - zesvětlit uvozovky kolem úryvků recenzí
    - doplnit odkazům „číst dál“ v recenzích znak `»` (pomocí pseudoelementu `::after`)

- **Obrázky jídel**
    - Pod každou sekci jídelního lístku umísti obrázek pokrmu. Nabídni prohlížečům 2 různé varianty (pomocí atributu `srcset`). Ale musíš si je nejdřív vytvořit.

---
---

## SVG: čištění kódu

Grafické editory ukládají do kódu SVG spoustu užitečných údajů ‒ pro další práci s kresbou **v grafickém editoru**. Na webu je ovšem tento kód spíš na obtíž. Zbytečně se zvětšuje velikost souboru, často se navíc jedná o kód, který je z hlediska prohlížeče vadný.

Můžeme se ho zbavit ručně, ale to je jednak pracné, jednak riskujeme, že smažeme něco důležitého. Jistější a jednodušší je tu činnost automatizovat a ručně případně ošetřit chyby u konkrétních kreseb. [Ideální nástroj je SVGOMG](https://jakearchibald.github.io/svgomg/), který umožňuje okamžitou vizuální kontrolu. Pokud se kresba pozmění nežádoucím způsobem, stačí pozměnit některá nastavení, dokud se zase nespraví.

Na stránce klikni na „Open SVG“ a v počítači vyber soubor, který chceš optimalizovat. Pokud se nezobrazí na šachovnicové ploše, zkus zatočit kolečkem myši a kresbu zvětšit tak, aby byla viditelná. Vpravo dole vidíš celkovou datovou úsprou. V pravém sloupci je pak celá řada nastavení. Většinou stačí je ponechat ve výchozím nastavení. Pozornost věnuj především těmto nastavením:

- `precision` rčuje počet destinných míst v atributech v SVG, jinými slovy zaokrouhluje hodnoty atributů. Přidáním desetinných míst se často kresba spraví (nemá většinou moc smysl zaokrouhlovat na více než 4 desetinná místa)
- `remove viewBox` vypnuto, protože `viewBox` ve většině případů chceme zachovat.
- `move attrs to parent group` pokud optimalizace kresbu poškodí, zkus vypnout toto nastavení, může pomoci.

## CSS hodnota `currentColor`

Hodnotu `currentColor` můžeme použít všude tam, kde lze nastavit danému prvku nějakou barvu. Tj. u vlastností `color`, `background-color`, `border-color`, `text-shadow`, `fill`, `stroke` atp. Jedná se svým způsobem o proměnnou, která nastaví barvu na barvu shodnou s barvou písma (`color`) rodičovského elementu.

Následující kód (vynchány jsou ostatní vlastnosti) nastaví barvu písma prvku `.highlights` na zelenou. Jelikož se barva písma dědí, zeleným písmem se vypíší i texty všech jeho potomků. Současně budou mít položyky seznamu stejnobarevné ohraničení a ikony tutéž barvu výplně. Pokud budeš chtít v budoucnu změnit barevné ladění této komponenty, změníš jen hodnotu `color` (barvu písma) rodičovského elementu `.highlights`.

```css

/* upoutavkove boxy */
.highlights {
    color: #009245; /* zelena barva italske vlajky */
}

/* upoutavkovy box */
.highlights li {
    border: 1px solid currentColor;
}

/* dekorativni ikona navrchu kazdeho boxu */
.highlights-icon {
    fill: currentColor;
}

```

## Pseudotřídy

Názvy tříd jsou záležitostí kódéra. Nicméně CSS nabízí tzv. pseudotřídy, které mají jednak předem definovaný název a stejně tak definovaný selektor, na který zacílí. Pseudotříd je celá řada, a některé už dokonce znáš. Jsou to pseudotřídy, které se vážou ke stavu odkazu (elementu `<a>`):

- `:link` odkaz
- `:visited` navštívený odkaz
- `:hover` odkaz pod kurzorem myši
- `:active` právě klikaný odkaz (pouze dokud se drží levé myšítko)
- `:focus` vybraný element (nejen odkaz, ale třeba formulářové políčko, ve které je právě kurzor)

### Použití

```css
a:visited {
    color: currentColor; /* navstivenemu odkazu nastav barvu okolniho textu */
}

a:hover {
    text-decoration: none; /* po najeti mysi skryj podtrzeni */
}
```

**Poznámka**: nevěnuj příliš pozornosti nastavování pseudotřídy `:hover`. Je sice poměrně efektní, ale s rozmachem dotykových zařízení pozbyla významu, na dotykové obrazovce `:hover` není (zjednodušeně řečeno).

Další užitečné pseudotřídy jsou `:first-child` `:last-child`. Jak název napovídá, vážou se k pořadí prvku ve struktuře HTML, přesněji k jejich pozici uvnitř rodičovského prvku.

```css

p:first-child {
    font-weight: bold;
}

p:last-child {
    margin-bottom: 0;
}

```

Předchozí pravidla změní odstavce v následující HTML struktuře tak, jak je popsáno v jednotlivých odstavcích.

```html

<section>
    <p>Tento je první, takže se vypíše tučně.</p>
    <p>Tento odstavec je obyčejným odstavcem.</p>
    <p>Tento rovněž</p>
    <p>Tento odstavec je poslední, a tudíž má vynulovaný spodní okraj.</p>
</section>

```

Pseudotřídy jsou velmi užitečnou pomůckou, která nás zbavuje nutnosti zakládat kvůli každé dílčí úpravě novou třídu.

Z dalších přseudotříd věnuj pozornost těmto (subjektivní výběr):

- univerzální ‒ `:not`, `:nth-child(n)`, `:nth-of-type(n)`
- formulářové ‒ `:checked`, `:enabled`,`:disabled`
- vícejazyčné weby ‒ `:lang`

Samostudium na [Je čas od Bohumila Jahody](http://jecas.cz/css-selektory)

## Pseudoelementy `::before` a `::after`

Jedná se o virtuální prvek, který se nenachází ve struktuře HTML, ale na stránce to vypadá, jako by tam byl. Zní to dost abstraktně a možná se ptáš, proč něco takového chtít, proč vlastně takový prvek nevytvořit. Je to proto, že základním kóderským principem je oddělovat obsah od formy.* Tj. mít někde nějaký HTML element, který má posloužit jako oddělovač, nebo dekorativní prvek, a přitom nenese žádný obsahový význam, je lépe ho nahradit právě pseudoelementem a přenést ho tak, kam patří, tj. do CSS.

A teď prakticky.

```css

blockquote::before {
    content: '„'; /* bez teto vlastnosti se pseudoelement nezobrazi */
}

```

Před prvkem `blockquote` se nyní zobrazí spodní uvozovka. Tu můžeme libovolně ostylovat pomocí běžných CSS vlastností. **Podstatné je uvést vlastnost `content`.** Dokude se neuvede, pseudoelement jako by nebyl. Stačí i s prázdnou hodnotou:

```css

blockquote::after {
    content: '';
}

```

Pak je ale nezbytné doplnit pseudoelementu rozměry (`width/height`), aby se na stránce něco objevilo. A protože to je možné jen u elementů blokových (a pseudoelementy jsou řádkové), musíš ještě změnit typ elementu pomocí vlastnosti `display`. Celé po kupě to může vypadat nějak takto:

```css

blockquote::after {
    content: '';
    display: block;
    width: 100%;
    height: 30px
    background-image: linear-gradient(to right, #009245 33.3333%, #fff 33.3333%, #fff 66.66666%, #ce2b37 66.66666%); /* skutecene barvy italske vlajky */
}

```

Předchozí pravidla nám za prvkem `blockquote` vytvoří pruh přes celou šířku prvku, vysoký 30 px, který bude mít na pozadí pruhy italské vlajky (za inspiraci díky Lucii).

Často se pseudoelementy používají k doplňování ikon k tlačítku a podobným účelům. Vyšší dívčí jsou pak různé triky, z nichž nejpopulárnější je tzv. [clearfix, který ošetřuje trable s plovoucími elementy](https://css-tricks.com/snippets/css/clear-fix/). S nástupem [flexboxu](http://jecas.cz/flexbox) ovšem jeho význam a všudypřítomnost poklesly, takže není potřeba se s ním zabývat, dokud to nebude potřeba.

*<small>V praxi se to často porušuje a to i proto, že současně toho nelze 100% dosáhnout. Ale to přenechme vášnivým debatám na toto téma na diskuzních fórech.</small>

## Relativní a absolutní pozicování

Někdy se může hodit přemístit prvek někam jinam, než kde se přirozeně nachází. V CSS k tomu máme vlastnost `position`. Ta může nabývat více hodnot, nás zatím zajímají 2: `relative` a `absolute`.

Počátek souřadnic, podle nichž se prvek pozicuje, záleží na zadaných souřadnicích. Vlastnostmi `top` a `left` pozicujeme od horního levého rohu, vlatnostmi `top` a `right` od pravého horního apod.

### Relativní pozicování

Prostým nastavením `position: relative;` se toho s prvkem mnoho nestane. Je třeba mu ještě předepsat, kam se má umístit. Nastavují se 4 souřadnice, resp. nětkeré z nich: `top`, `right`, `bottom`, `left`. Hodnotami mohou být jakékoli jednotky, které v CSS používáme, tj. `px`, `em`,`rem`, `%`. Prvek se posune z místa, v němž se nachází o tolik, kolik mu předepíšeme. Můžeme zadávat i záporné hodnoty. I když se prvek posune, jeho původní umístění mu zůstává vyhrazeno (což je podsatný rozdíl oproti pozicování absolutnímu).

```css

p {
    position: relative;
    top: 1em;
    left: -2em;
}

```

### Absolutní pozicování

Nastavením `position: absolute;` a doplněním souřadnic se prvek rovněž posune o dané hodnoty, ale se zásadními rozdíly. Prvním je fakt, že prvek se v tomto případě jakoby vystřihne a jeho místo přirozeně zaplní okolní obsah, jako kdyby tam absolutně pozicovaný prvek nikdy nebyl. Toho lze docílit i pouhým nastavením `position: absolute;` bez uvedení souřadnic.

Dalším důležitým rozdílem je otázka, odkud vycházejí souřadnice pozicovaného prvku, tj. pokud nastavím `left: 20px;`, okdud se těch 20 px počítá. V případě relativního prohlížeč odměří 20 pixelů od původního umístění a je to. V případě absolutního si prohlížeč nejprve najde nejbližší rodičovský prvek, **který má nastavenu `position: relative;`** a pokud ho nenajde, posune počátek souřadnic do levého horního rohu prvku body.

Takže se nelekni, když ti prvek nastavením absolutní pozice „zmizí“. Prostě se jen posunul vzhledem k rodiči, který je zrovna mimo obrazovku.

Za zmínku ještě stojí hodnota `position: static;` tím se zruší veškeré pozicování a prvek se vrátí zpátky na své místo. Využívá se k přebíjení ostatních hodnot.

Další a podrobnější informace opět na [Je čas Bohumila Jahody](http://jecas.cz/position).

## Media queries

S rozmachem mobilních zařízení s nejrozmanitějšími velikostmi (a jinými vlastnostmi!) obrazovek vznikla potřeba omezit určitá CSS pravidla jen pro určité jejich skupiny. Proto do specifikace CSS přibyly tzv. media queries, která přesně vymezí typ a vlastnosti zařízení pro něž se daná pravidla uplatní. Nejjednodušší příklad:

```css
/* na tiskovych zarizenich, rozumej tiskarnach, odstavce vypis (vytiskni) cernou barvou ‒ setrime tonery */
@media print {

    p {
        color: #000;
    }
}

```

Mimochodem, na tiskové styly bychom neměli zapomínat. Jednak jsou uživatelé, kteří si stránky tisknou (z nejrůznějších důvodů), jednak to někdy může dávat smysl (např. jídelní lístek). V tiskových stylech pak skryjeme záhlaví a zápatí (pomocí `display: none;`) a nastavíme barvy na černobílé.

Nejčastější vlastností, na kterou pomocí media queries cílíme, bývá šířka.

```css

@media only screen and (min-width: 1600px) {

    html {
        font-size: 20px;
    }
}

```

Předchozí zápis nastavuje výchozí velikost písma 20 px pro všechny obrazovky širší než 1600 pixelů.

Jednotky můžeme použít i relativní.

```css

@media only screen and (min-width: 50em) {

    html {
        font-size: 20px;
    }
}

```

Stejně tak můžeme omezovat pravidla opačně, tj. do maximální šířky zařízení.


```css

/* Do sirky obrazovky 1599px nastav vychozi velikost pisma 16 px */
@media only screen and (max-width: 1599px) {

    html {
        font-size: 16px;
    }
}

```

Povolené jsou i tzv. logické operátory. Operátor `a` zapíšeme následovně.

```css

/* Plati na obrazovkach v rozmezi 400 a 799 px vcetne. */
@media only screen and (min-width: 400px) and (max-width: 799px) {

    article {
        width: 100%;
    }
}

```

Operátor `nebo` se zapíše čárkou.

```css

/* Plati na obrazovach do sirky 799px nebo sirsich nez 1200px */
@media only screen and (max-width: 799px), (min-width: 1200px) {

    article {
        width: 50%;
    }
}

```

### Vlastnost `box-sizing`

Responzivní ladění nám zjednodušuje (ne-li přímo umožňuje) klíčová vlastnost `box-sizing` s hodnotou `border-box`. Ta nastaví, že do šířky (`width`) elementu se započítává jak jeho `padding` tak i tloušťka ohraničení (`border-width`). Pokud tě zajímá proč, přečti si [článek na webu Je čas](http://jecas.cz/box-sizing). V každém případě si na začátek stylopisu přidej následující kód:

```css

html {
    box-sizing: border-box;
}

*,
*::before,
*::after {
    box-sizing: inherit;
}

```

Vlastnost `box-sizing` se dědí, takže ji stačí nastavit takto pro všechny elementy na stránce. Nezapomínáme přitom ani na pseudoelementy `::before` a `::after`.

### Meta tag viewport

Posledním předokladem pro úspěšné responzivní ladění je tag, který musí být přítomen v hlavičce stránky. Bez něj se web při změně rozměrů okna nebude chovat responzivně.

```html

<meta name="viewport" content="width=device-width, initial-scale=1.0">

```

Pokud tě zajímá proč, přečti si teorii na stránce [Je čas](http://jecas.cz/meta-viewport).


---

## Kam dál

- [Hvězdičkové hodnocení s poměrným podbarvením hvězd (pomocí gradientu)](http://www.vzhurudolu.cz/prirucka/reseni-svg-hodnoceni)
- [Rozkrytí souřadnicových systémů v SVG (`viewBox` a viewport)](https://www.sarasoueidan.com/blog/svg-coordinate-systems/) od Sary Soueidan, která je světovou kapacitou na SVG

## Zdroje

- [SVGOMG ‒ online úprava SVG](https://jakearchibald.github.io/svgomg/)
- [SVG (nejen) ikony](https://www.iconfinder.com/)
- [Bulk Resize Photos](https://bulkresizephotos.com/) ‒ hromadná úprava obrázků na zadaný rozměr
- [TinyJPG/PNG](https://tinyjpg.com/) ‒ hromadná komprese obrázků
- [Coolors.co](https://coolors.co/) ‒ jednoduchý generátor barevných schémat
- [Paletton.com](http://paletton.com/) ‒ pokročilý generátor barevných schémat
[Příklad na media queries](https://css-tricks.com/css-media-queries/) ‒ stačí se podívat na screenshoty na dané stránce, a porovnat s připojeným kódem. Nemusíš číst celý článek.
