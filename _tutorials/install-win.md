---
title:  Návod na instalaci Atomu
date:   2017-05-10
---

Pro tvorbu stránek potřebujeme vhodný textový editor. V našem případě budeme používat Atom.

## Windows
### Stažení
Atom lze stáhnout ze stránek http://atom.io. Klikni na červené tlačítko a stáhni instalační soubor.
![Stažení atomu ze stránek atom.io](../assets/install/install-win-download.png)

### Instalace
Otevři stažený soubor a počkej, než se Atom nainstaluje.
![Spuštění souboru](../assets/install/install-win-install.png)


### Spuštění programu
Program by se měl po dokončení instalace spustit. Pokud ne, tak ho spusť. Zobrazí se uvítací obrazovka.
![Spuštění souboru](../assets/install/install-win-welcome.png)

### Instalace balíčků
Program je nyní nainstalovaný a připravený k použití. Atom je poměrně jednoduchý editor a pro pokročilejší funkce a pohodlnější práci slouží rozšíření, která se instalují pomocí balíčků (package).

Pro naše účely se hodí *emmet* (chytřejší editace nejen html a css) a *atom-beautify* (lepší odsazení a formátování kódu). Klikni na tlačítko instalovat balíček (Install a package) na kartě Welcome Guide. Balíčky lze instalovat případně i v nastavení (Settings -> Install).

Ve vyhledávači najdi název. U nalezeného balíčku stiskni tlačítko instalovat.
![Spuštění souboru](../assets/install/install-win-package.png)

Nyní máš připravený editor pro lepší a pohodlnější editaci kódu.

[zpět na úvod](/)
