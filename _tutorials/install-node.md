---
title:  Návod na instalaci Node.js
date:   2017-05-10
---

Pro efektivnější práci na frontendu potřebujeme lepší vývojové prostředí. K tomu nám pomůže Node.js

## Windows

### Node.js
Na Windows si stáhneme a nainstalujeme instalační balíček _Windows Installer_ ze stránek [node.js](https://nodejs.org/en/download/).

### Cmder
Příkazová řádka na windows je pro naše účely poměrně nešikovná. Proto si stáhneme emulátor konzole pro windows ze stránek [cmderu](http://cmder.net).

Přibližně v polovině stránky najdete dva odkazy ke stažení. Stáhněte si plnou verzi a rozbalte ji. V rozbaleném archivu najdete soubor cmder.exe. Spusťte ho s administrátorskými právy.

### Funguje to?

Pokud jste udělali vše správně, pak by se po zadání následujícího příkazu do cmderu měla zobrazit verze (pár čísílek oddělených tečkami).

```bash
node -v
```

## Mac
### XCode
Nejprve je potřeba nainstalovat XCode. Najdete ho na AppStoru.

### Homebrew
Následně potřebujeme balíčkový manager. To je program, kterému řekneme, co chceme nainstalovat a on za nás najde nejnovější verzi a nainstaluje ji. Na Macu se nejvíce používá Homebrew.

```bash
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

### Node.js
Nyní nainstalujeme pomocí homebrew node.js

```bash
brew install node
```

### Funguje to?

Pokud jste udělali vše správně, pak by se po zadání následujícího příkazu měla zobrazit verze (pár čísílek oddělených tečkami).

```bash
node -v
```


[zpět na úvod](/)
