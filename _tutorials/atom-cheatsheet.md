---
title:  Klávesové zkratky - Atom
date:   2017-05-22
---

Atom má mnoho klávesových zkratek, které je dobré si zapamatovat (nebo alespoň vědět, že existují).

## Proč?

Klávesové zkratky nám šetří práci i čas strávený hledáním funkcí v aplikaci. Občas to také bývá jediný způsob, jak nějakou akci spustit.

## Seznam zkratek

### Připnutí složky
Otevře složku a připne ji na *Tree View* vlevo
- Win: `ctrl`+`shift`+`O`
- Mac: `cmd`+`shift`+`O`

Zobrazit/schovat *Tree View*
- Win: `ctrl`+`\`
- Mac: `cmd`+`\`

### Paleta příkazů
Otevře paletu příkazů, kde lze vyhledávat všechny funkce atomu a nainstalovaných pluginů.
- Win: `ctrl`+`shift`+`P`
- Mac: `cmd`+`shift`+`P`

### Práce se soubory
Vytvoří nový soubor
- Win: `ctrl`+`N`
- Mac: `cmd`+`N`

Uloží právě otevřený soubor
- Win: `ctrl`+`S`
- Mac: `cmd`+`S`

### Úprava a formátování textu
Přeformátovat text
- `ctrl`+`alt`+`B`

Zduplikovat řádek
- Win: `ctrl`+`shift`+`D`
- Mac: `cmd`+`shift`+`D`

Označit další stejné slovo
- Win: `ctrl`+`D`
- Mac: `cmd`+`D`

### Změny v kódu
Zakomentovat řádek
- Win: `ctrl`+`/`
- Mac: `cmd`+`/`

Obalit vybraný text elementem
- `alt`+`shift`+`W`

Vyhodnotit emmet výraz ([Tahák emmet výrazů](https://docs.emmet.io/cheat-sheet/))
- Kurzor na konci výrazu +`tab`

[zpět na úvod](/)
