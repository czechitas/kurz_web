---
title:  Podklady
date:   2017-05-24
---

Tady najdeš podklady ke cvičením.

<!-- ## [6. lekce](../lessons/6.html) -->
## 6. lekce

### Podklady

- [FancyBox](http://fancyapps.com/fancybox/3/)
- [Pingy CLI](https://pin.gy/cli/)
- [Mozila Developer Network (MDN)](https://developer.mozilla.org) ‒ zdroj ucelených informací o HTML, CSS, JS (a dalších)

### Projekt

- [SCSS - stylopisy rozlámané do komponent](../assets/downloads/lekce_06/scss.zip)
- [Al Dente s FancyBoxem](../assets/downloads/lekce_06/aldente-fancybox.zip)
- [Al Dente s Pingy](../assets/downloads/lekce_06/aldente-pingy.zip)

---

## [5. lekce](../lessons/5.html)

### Podklady

- [Responzivní obrázky naplno](http://www.vzhurudolu.cz/prirucka/srcset-sizes) ‒ všechny možnosti atributů `srcset` a `sizes`
- [Element picture](http://www.vzhurudolu.cz/prirucka/picture)

- [Kapitola o flexboxu](http://jecas.cz/flexbox) na Ječas.cz od Bohumila Jahody
- [Přehledně o flexboxu s obrázky](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) (anglicky)
- [Flexbox Froggy](http://flexboxfroggy.com/) ‒ trénink flexboboxových vlastností hravou formou

### Projekt

- [Projekt rozdělený na stránky online](../assets/downloads/lekce_05/aldente/)
- [Projekt rozdělený na stránky ke stažení (ZIP)](../assets/downloads/lekce_05/aldente.zip)

- [Projekt s flexboxem online](../assets/downloads/lekce_05/aldente-flexbox/)
- [Projekt s flexboxem ke stažení (ZIP)](../assets/downloads/lekce_05/aldente-flexbox.zip)

- [Projekt s galerií online](../assets/downloads/lekce_05/aldente-gallery/)
- [Projekt s galerií ke stažení (ZIP)](../assets/downloads/lekce_05/aldente-gallery.zip)

---

## [4. lekce](../lessons/4.html)

### Podklady

- [Obrázky](../assets/downloads/lekce_03/fotky.zip)
- [Ikona chilli](../assets/downloads/lekce_03/chilli.svg)
- [SVGOMG ‒ online úprava SVG](https://jakearchibald.github.io/svgomg/)
- [Zmenšení obrázků online](https://bulkresizephotos.com)
- [TinyJPG/PNG](https://tinyjpg.com/) ‒ hromadná komprese obrázků

### Projekt

- [DÚ ‒ Ikona hvězda](../assets/downloads/lekce_03/star.svg)
- [Projekt online](../assets/downloads/lekce_04/projekt/)
- [Projekt ke stažení (ZIP)](../assets/downloads/lekce_04/aldente-po-4-lekci.zip)

---

## [3. lekce](../lessons/3.html)

### Podklady

- [Logo](../assets/downloads/lekce_03/al-dente_logo.svg)
- [Ikona vege](../assets/downloads/lekce_03/vege.svg)
- [Ikona chilli ‒ vyčištěná](../assets/downloads/lekce_03/chilli-clean.svg)

### Projekt

- [Základní CSS s vygenerovaným barevným schématem](../assets/downloads/lekce_03/styles-base.css)
- [CSS s gradientem](../assets/downloads/lekce_03/styles-gradient.css)
- [CSS s ikonami](../assets/downloads/lekce_03/styles-icons.css)
- [HTML s logem a ikonami po 3. lekci](../assets/downloads/lekce_03/web-icons.html)
- [Projekt online](../assets/downloads/lekce_03/projekt/)
- [Projekt ke stažení (ZIP)](../assets/downloads/lekce_03/aldente-po-3-lekci.zip)

---

## [2. lekce](../lessons/2.html)

### Podklady

- [Texty](../assets/downloads/lekce_02/texty.docx)
- [Blanka ‒ základní sjednocovací CSS](../assets/downloads/lekce_02/blanka.css) od Martina Michálka
- [Reboot ‒ komplexnější sjednocovací CSS](../assets/downloads/lekce_02/reboot.css) od Bootstrapu

### Projekt

- [Hotové HTML](../assets/downloads/lekce_02/otagovano.html)
- [DÚ ‒ stránka recenzí](../assets/downloads/lekce_02/recenze.docx)
