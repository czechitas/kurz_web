---
title:  Návod na instalaci Atomu
date:   2017-05-10
---

Pro tvorbu stránek potřebujeme vhodný textový editor. V našem případě budeme používat Atom.

## Mac
### Stažení
Atom lze stáhnout ze stránek http://atom.io. Klikni na červené tlačítko a stáhni aplikaci Atom.
![Stažení atomu ze stránek atom.io](../assets/install/install-mac-download.png)

### Instalace
Přesuň staženou aplikaci do složky Applications.
![Spuštění souboru](../assets/install/install-mac-move-to-apps.png)

První spuštění prověď kliknutím pravým tlačítkem (nebo ctrl a klik) na soubor a vyber možnost otevřít (Open).
![Spuštění souboru](../assets/install/install-mac-first-launch.png)

Zobrazí se hláška, že tento soubor je stažený z internetu a může být nebezpečný. My jsme ovšem ochotni toto nebezpečí podstoupit, proto klikneme na tlačítko otevřít (Open). Pro více informací si můžete přečíst [tento](https://support.apple.com/kb/ph18657?locale=cs_CZ) článek.
![Spuštění souboru](../assets/install/install-mac-allow.png)


### Spuštění programu
Po spuštění programu bychom měli vidět úvodní obrazovku
![Spuštění souboru](../assets/install/install-mac-welcome.png)

### Instalace balíčků
Program je nyní nainstalovaný a připravený k použití. Atom je poměrně jednoduchý editor a pro pokročilejší funkce a pohodlnější práci slouží rozšíření, která se instalují pomocí balíčků (package).

Pro naše účely se hodí *emmet* (chytřejší editace nejen html a css) a *atom-beautify* (lepší odsazení a formátování kódu). Klikni na tlačítko instalovat balíček (Install a package) na kartě Welcome Guide. Balíčky lze instalovat případně i v nastavení (Settings -> Install).

Ve vyhledávači najdi název. U nalezeného balíčku stiskni tlačítko instalovat.
![Spuštění souboru](../assets/install/install-mac-package.png)

Nyní máš připravený editor pro lepší a pohodlnější editaci kódu.

[zpět na úvod](/)
