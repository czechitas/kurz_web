---
title:  Návody
date:   2017-05-10
---

Tady najdeš návody a nástroje, se kterými budeme pracovat. Je to vlastně taková bedýnka s nářadím.

## Instalace
- [Instalace Atomu - Windows](/tutorials/install-win.html)
- [Instalace Atomu - Mac](/tutorials/install-mac.html)
- [Node.js - Windows &amp; Mac](/tutorials/install-node.html)

## Nástroje
- [Developer Tools (Chrome)](/tutorials/devtools.html)

## HTML
- [Formuláře](/tutorials/form.html)

## Taháky
- [Klávesové zkratky - Atom](/tutorials/atom-cheatsheet.html)
- [Tahák - Emmet výrazy](https://docs.emmet.io/cheat-sheet/)
